= Osobní deník

== Informace

(C) 2016 Tomáš Raděj

Vydáno pod licencí https://creativecommons.org/licenses/by/4.0/[CC BY 4.0].

Otevřete programem LibreOffice Draw. Je třeba mít nainstalovaný font
https://www.fontsquirrel.com/fonts/almendra[Almendra].

Tento osobní deník je redukovanou formou standardních deníků na Dračí doupě, kde se nehraje striktně podle pravidel. Upravte dle libosti, aby vyhovoval vašemu hernímu stylu.

== Tisk

Předpokládaná metoda tisku je 2 strany na A4, zleva doprava. Tento dokument počítá s okrajem 5 mm tiskové plochy tiskárny od okraje papíru, takže budete-li tisknout jinak, okraje nemusí odpovídat představám.

Máte-li jen jednostrannou tiskárnu, tiskněte nejdříve strany 1, 2, 5, 6, pak vložte vytištěné papíry (bez přerovnávání) zpět do tiskárny a vytiskněte strany 3, 4, 7, 8 _v opačném pořadí_ (reversed).
